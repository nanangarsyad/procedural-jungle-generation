////////////////////////////////////////////////////////////////////////////////
//
// (C) Andy Thomason 2012-2014
//
// Modular Framework for OpenGLES2 rendering on multiple platforms.
//
namespace octet {
  /// Scene containing a box with octet.
  class camera_app : public app {
    // scene for drawing box
    ref<visual_scene> app_scene;

    CameraKeyboardHandler _camera;

    mat4t& getCameraToWorld() const {
      // IMPORTANT Use get_node(). get_scene_node() is something different!
      return app_scene->get_camera_instance(0)->get_node()->access_nodeToParent();
    };

    void handleInput() {
      _camera.update();
    };

  public:
    /// this is called when we construct the class before everything is initialised.
    camera_app(int argc, char **argv) : app(argc, argv) {
    }

    /// this is called once OpenGL is initialized
    void app_init() {
      app_scene =  new visual_scene();
      app_scene->create_default_camera_and_lights();

      // IMPORTANT initialize camera correctly
      _camera.attach(this);
      _camera.setCamera(&getCameraToWorld());

      ref<image> materialImage(new image("assets/andyt.gif"));

      material *red = new material(materialImage);
      mesh_box *box = new mesh_box(vec3(4, 8, 12));
      scene_node *node = new scene_node();
      app_scene->add_child(node);
      app_scene->add_mesh_instance(new mesh_instance(node, box, red));
    }

    /// this is called to draw the world
    void draw_world(int x, int y, int w, int h) {
      handleInput();

      int vx = 0, vy = 0;
      get_viewport_size(vx, vy);
      app_scene->begin_render(vx, vy);

      // update matrices. assume 30 fps.
      app_scene->update(1.0f/30.0f);

      // draw the scene
      app_scene->render((float)vx / vy);

      // tumble the box  (there is only one mesh instance)
      scene_node *node = app_scene->get_mesh_instance(0)->get_node();
      node->rotate(1, vec3(1, 0, 0));
      node->rotate(1, vec3(0, 1, 0));
    }
  };
}
