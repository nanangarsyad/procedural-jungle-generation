//
// Unity Build for poly2tri
//

#undef EPSILON

#include "common/shapes.cc"

#include "sweep/advancing_front.cc"
#include "sweep/cdt.cc"
#include "sweep/sweep.cc"
#include "sweep/sweep_context.cc"

// btConvexHull.cpp
#define EPSILON btScalar(0.000001)