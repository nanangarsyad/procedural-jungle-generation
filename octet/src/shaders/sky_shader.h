////////////////////////////////////////////////////////////////////////////////
//
// (C) Andy Thomason 2012, 2013
//
// Modular Framework for OpenGLES2 rendering on multiple platforms.
//
// Solid color shader
//
// shader for the sky

namespace octet { namespace shaders {
  class sky_shader : public shader {
    // indices to use with glUniform*()

    // index for model space to projection space matrix
    GLuint modelToProjectionIndex_;

    // index for flat shader emissive color
    GLuint emissive_colorIndex_start;
    GLuint emissive_colorIndex_end;
    GLuint _emissiveColorEndAlternateIndex;
    GLuint time_Index;
    GLuint _cycleDurationIndex;
    GLuint fog_index;


    vec4 emissive_color_start; 
    // Sky colour for the beginning of the day
    vec4 emissive_color_end;
    // Alternate sky colour for end of day
    vec4 _emissiveColourEndAlternate;

    static float time;
    // Duration of a day-night cycle
    float _cycleDuration;

  public:
    void setColors(const vec4 &colorStart, const vec4 &colorEnd, const vec4& colorEndAlternate){
      emissive_color_start = colorStart; 
      emissive_color_end = colorEnd;
      _emissiveColourEndAlternate = colorEndAlternate;
    }

    float getTime() const {
      return time;
    };

    void resetTime() {
      time = 0;
    };

    float getCycleDuration() const {
      return _cycleDuration;
    };

    void setCycleDuration(float cycleDuration) {
      _cycleDuration = cycleDuration;
    };

    void init() {
      time = 0;
      _cycleDuration = 30.f;

      emissive_color_start = vec4(1.0f, 1.0f, 1.0f, 1.0f); 
      emissive_color_end = vec4(0.0f, 0.0f, 1.0f, 1.0f);
      _emissiveColourEndAlternate = vec4(0.0f, 0.0f, 0.0f, 1.0f);

      // this is the vertex shader. main() will be called for every vertex
      // of every triangle. The output is gl_Position which is used to generate
      // 2D triangles in the rasterizer.
      
      const char vertex_shader[] = SHADER_STR(
        attribute vec4 pos;
        uniform mat4 modelToProjection;
        varying float positionY;
        varying vec4 pos_;
        void main() { gl_Position = modelToProjection * pos; positionY = pos.y; pos_ = pos;}
      );

      // this is the fragment shader. It is called once for every pixel
      // in the rasterized triangles.
      const char fragment_shader[] = SHADER_STR(
        uniform vec4 emissive_color_start;
        uniform vec4 emissive_color_end;

        uniform vec4 emissive_color_end_alternate;
        uniform float time;
        uniform float cycle_duration;
        uniform int fog;
        varying float positionY;
        varying vec4 pos_;


        float Noise(int x, int y) {
        	        vec2 pos = vec2(x, y);
        	        return fract( sin( dot(pos*0.001 ,vec2(24.12357, 36.789) ) ) * 12345.123);	
        }
        
        float Interpolate(float x, float y, float a) {
                  float negA = 1.0 - a;
                  float negASqr = negA * negA;
                  float fac1 = 3.0 * (negASqr) - 2.0 * (negASqr * negA);
                  float aSqr = a * a;
                  float fac2 = 3.0 * aSqr - 2.0 * (aSqr * a);
        
                  return x * fac1 + y * fac2;
        }
        
        float GetValue(float x, float y) {
                  int Xint = int(x);
                  int Yint = int(y);
                  float Xfrac = x - float(Xint);
                  float Yfrac = y - float(Yint);
        
                  //noise values
                  float n01 = Noise(Xint-1, Yint-1);
                  float n02 = Noise(Xint+1, Yint-1);
                  float n03 = Noise(Xint-1, Yint+1);
                  float n04 = Noise(Xint+1, Yint+1);
                  float n05 = Noise(Xint-1, Yint);
                  float n06 = Noise(Xint+1, Yint);
                  float n07 = Noise(Xint, Yint-1);
                  float n08 = Noise(Xint, Yint+1);
                  float n09 = Noise(Xint, Yint);
                  
                  float n12 = Noise(Xint+2, Yint-1);
                  float n14 = Noise(Xint+2, Yint+1);
                  float n16 = Noise(Xint+2, Yint);
                  
                  float n23 = Noise(Xint-1, Yint+2);
                  float n24 = Noise(Xint+1, Yint+2);
                  float n28 = Noise(Xint, Yint+2);
                  
                  float n34 = Noise(Xint+2, Yint+2);
        
                  //find the noise values of the four corners
                  //corners, left-right, center
                  float x0y0 = 0.0625*(n01+n02+n03+n04) + 0.125*(n05+n06+n07+n08) + 0.25*(n09); 
                  //left-right-(left-right-under 2 lines), (left-right-under 1 line)-center-(center under 2 lines), center under 1 line
                  float x1y0 = 0.0625*(n07+n12+n08+n14) + 0.125*(n09+n16+n02+n04) + 0.25*(n06);  
                  float x0y1 = 0.0625*(n05+n06+n23+n24) + 0.125*(n03+n04+n09+n28) + 0.25*(n08);  
                  float x1y1 = 0.0625*(n09+n16+n28+n34) + 0.125*(n08+n14+n06+n24) + 0.25*(n04);  
        
                  //interpolate between those values according to the x and y fractions
                  float v1 = Interpolate(x0y0, x1y0, Xfrac); //interpolate in x direction (y)
                  float v2 = Interpolate(x0y1, x1y1, Xfrac); //interpolate in x direction (y+1)
                  float fin = Interpolate(v1, v2, Yfrac);  //interpolate in y direction
        
                  return fin;
        }
        
        //float randomseed = 0.0;
                   
        float Total(float i, float j, float _time) {
                  //properties of one octave (changing each loop)
                  float persistence = 0.4;
                  float freq = 15.0;
                  float _amplitude  = 0.6;
                  //int octaves = 2;
                  float randomseed = _time;
                  float t = 0.0;
                  
                  for(int k = 0; k < 5; k++) 
                  {
                    t = t + GetValue(j * freq, i * freq + randomseed) * _amplitude;
                    _amplitude = _amplitude * persistence;
                    freq = freq * 2.0;
                  }
        
                  return t;
        }

        float Exp(float x) {
	        float cover = 0.009;
	        float sharp = 0.11;
	        x = x - cover;
	        if (x < 0.0) x = 0.0;
	        float density = 1.0 - pow(sharp,x);
	        return density;
        }
        
        //vec4 emissive_color_start;
        //vec4 emissive_color_end;


        float pi()
        {
          return 3.1415926535897932384626433832795;
        }

        float RemapValClamped(float flInput, float flInLo, float flInHi, float flOutLo, float flOutHi)
        {
          if (flInput < flInLo)
            return flOutLo;
          if (flInput > flInHi)
            return flOutHi;
          return (((flInput-flInLo) / (flInHi-flInLo)) * (flOutHi-flOutLo)) + flOutLo;
        }
        
        float surroundingIslands(vec4 position){
          if ((position.x<-100.0 && position.x>-600.0)&&(position.y>-20.0)){
           return (20.0 * sin(6.28*position.x/450.0) - 9.0 + 4.0 * sin(6.28*position.x/130.0+20.0)+10.0);
          }
          else if(position.z<300.0 && position.z>-100.0 && position.y>-20.0 && position.x>0.0){
            return (20.0 * sin(6.28*position.z/450.0) - 9.0 + 4.0 * sin(6.28*position.z/130.0+20.0)+10.0+3.0 * sin(6.28*position.z/70.0+20.0)+10.0);
          }
          return -50.0;
        }

        void main(void)
        {
             
        		 //randomseed = time;
        		 //emissive_color_start = vec4(1, 1, 1, 1);
        		 //emissive_color_end = vec4(0, 0, 1, 1);
        		 vec2 position = gl_FragCoord.xy / vec2(512.0,512.0) * 0.5;
             float n = Exp(Total(position.x, position.y, time));
                  
            	 //if (n > 1.0) n = 1.0;
            	 //else if (n < 0.0) n = 0.0;
        		 
        		 //liniar interpolation
            	 //gl_FragColor = (1.0 - n)*emissive_color_start + n*emissive_color_end;
        	
        		 //cosine interpolation
        		 //float ft = n * 3.1415927;
        		 //float f = (1.0 - cos(ft)) * 0.5;			
        		 //gl_FragColor = emissive_color_start*(1.0-f) + emissive_color_end*f;
        	
        		 //cubic interpolation
        		 float p = 3.0 * n * n - 2.0 * n * n* n;

             // Applying sinusodial interpolation to define current sky background colour based on time and cycle duration
             // pi / 2 is required to map start point to 0 and end point to 1 of sin wave
             // abs() is required to keep values in the +ve y region (i.e. 0 - 1)
             vec4 sky = mix(emissive_color_end, emissive_color_end_alternate, ( cycle_duration < 0.0 ? 0.0 : abs(sin(time / cycle_duration * pi() * 0.5))) );

             if (positionY < surroundingIslands(pos_)){
               if (positionY < -9.5){
                  gl_FragColor = vec4(0.7, 0.5, 0.0, 1.0);
                }
                else if (positionY >= -9.5 && positionY < -7.5){
                  gl_FragColor = mix(vec4(0.7, 0.5, 0.0, 1.0), vec4(0.0, 0.1, 0.0, 1.0),(positionY+9.5)/2.0);
                }
                else{
                  gl_FragColor = vec4(0.0, 0.1, 0.0, 1.0);
                }
             }
             else{
        		    gl_FragColor = (1.0 - p) * emissive_color_start + n * sky;
             }
             if ( fog != 0 ){
              float flY = RemapValClamped(positionY, 0.0, 500.0, 0.0, 1.0);
              gl_FragColor += vec4(0.46, 0.5, 0.55, 1.0)*(1.0-flY);
             }

             //the axis of the sphere
             /*if ( abs(pos_.x - 1.0) < 0.5 || abs(pos_.y -1.0) < 0.5 || abs(pos_.z -1.0) < 0.5 || abs(pos_.x - 500.0)<0.5){
              gl_FragColor = vec4(0,1,0,1);
             }*/

        }
      );
    
      // compile and link the shaders
      shader::init(vertex_shader, fragment_shader);

      // set up handles to access the uniforms.
      modelToProjectionIndex_ = glGetUniformLocation(program(), "modelToProjection");
      emissive_colorIndex_start = glGetUniformLocation(program(), "emissive_color_start");
      emissive_colorIndex_end = glGetUniformLocation(program(), "emissive_color_end");
      _emissiveColorEndAlternateIndex = glGetUniformLocation(program(), "emissive_color_end_alternate");
      time_Index = glGetUniformLocation(program(), "time");
      _cycleDurationIndex = glGetUniformLocation(program(), "cycle_duration");
      fog_index = glGetUniformLocation(program(), "fog");

    }

    

    // start drawing with this shader
    void render(const mat4t &modelToProjection, const mat4t &modelToCamera, const vec4 *light_uniforms, int num_light_uniforms, int num_lights, int fog = 0) {
      // start using the program
      shader::render();
      time += 0.1f;

      // set the uniforms.
      glUniform4fv(emissive_colorIndex_start, 1, emissive_color_start.get());
      glUniform1f(time_Index, time);
      glUniform1f(_cycleDurationIndex, _cycleDuration);
      glUniform4fv(emissive_colorIndex_end, 1, emissive_color_end.get());
      glUniform4fv(_emissiveColorEndAlternateIndex, 1, _emissiveColourEndAlternate.get());
      glUniformMatrix4fv(modelToProjectionIndex_, 1, GL_FALSE, modelToProjection.get());
      glUniform1i(fog_index, fog);

      // now we are ready to define the attributes and draw the triangles.
    }
  };

  float sky_shader::time=0;
}}
