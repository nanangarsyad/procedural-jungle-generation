namespace octet {
  namespace math {

    // Random Number Generation in C++: http://www.johndcook.com/test_TR1_random.html

    class RandomNumberGenerator {
    public:
      virtual ~RandomNumberGenerator() { };

      virtual unsigned int getMin() = 0;
      virtual unsigned int getMax() = 0;

      virtual unsigned int operator()() = 0;
    };

    // --

    class LinearCongruential : public RandomNumberGenerator {
    private:    
      LinearCongruential() {
      };

      // non-copyable entity
      LinearCongruential(const LinearCongruential&);
      LinearCongruential& operator=(const LinearCongruential&);

    public:
      void setSeed(unsigned int seed) {
        ::srand(seed);
      };

      unsigned int getMin() {
        return 0;
      };
    
      unsigned int getMax() {
        return RAND_MAX;
      };

      unsigned int operator()() {
        return ::rand();
      };

      static LinearCongruential& getInstance() {
        static LinearCongruential rand;
        return rand;
      };
    };

    // --
  
    class OctetRand : public RandomNumberGenerator {
    private:
      octet::random _rand;

    public:
      OctetRand() {
      };

      explicit OctetRand(unsigned int seed) :
        _rand(seed) {
      };

      unsigned int getMin() {
        return 0;
      };
    
      unsigned int getMax() {
        return 2;
      };

      unsigned int operator()() {
        return _rand.get((int) getMin(), (int) getMax());
      };
    };

    // --

    // Reference: http://en.wikipedia.org/wiki/Xorshift
    class XorShift : public RandomNumberGenerator {
    public: 
      typedef uint32_t Seed;

    private:
      Seed _x, _y, _z, _w;

    public:
      explicit XorShift(Seed x = 123456789, Seed y = 362436069, Seed z = 521288629, Seed w = 88675123) :
        _x(x),
        _y(y),
        _z(z),
        _w(w) {
      };

      unsigned int getMin() {
        return 0;
      };
    
      unsigned int getMax() {
        return UINT_MAX;
      };

      unsigned int operator()() {
        uint32_t t = _x ^ (_x << 11);
        _x = _y; _y = _z; _z = _w;
        return _w = _w ^ (_w >> 19) ^ (t ^ (t >> 8));
      };
    };
  
    float randf(octet::RandomNumberGenerator* rand) {
      return rand == NULL ? 0.f : (((float) (*rand)()) / ((float) (rand->getMax() - rand->getMin())));
    };
  
    float randf(octet::RandomNumberGenerator* rand, float min, float max) {
      return (randf(rand) * (max - min)) + min;
    };

  } // namespace math
} // namespace octet
