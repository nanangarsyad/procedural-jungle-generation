////////////////////////////////////////////////////////////////////////////////
//
// Space Monkey Mafia - Procedural Jungle Generation
//
// URL: https://www.doc.gold.ac.uk/~mas01at/proj2013/?page_id=4
//
// Artemis Tsouflidou     [ma303at]
// Brian Gatt             [ma301bg]
// Stefana Ovesia         [ma301so]
// Gustavo Arcanjo Silva  [io301gas]
//
////////////////////////////////////////////////////////////////////////////////

#ifndef _SMM_JUNGLE_INCLUDED_H_
#define _SMM_JUNGLE_INCLUDED_H_

#include "utilities.h"

#include "perlin_noise.h"

#include "camera.h"
#include "camera_handler.h"

#include "mesh.h"
#include "handle_mesh.h"
#include "read_file.h"
#include "Catmull_Rom_Spline.h"
#include "mesh_engine.h"
#include "audio_manager.h"

#include "subdivision.h"
#include "triangulation.h"

#include "texture_atlas.h"

#include "state.h"
#include "rain.h"

#include "jungle_app.h"

#endif // _SMM_JUNGLE_INCLUDED_H_