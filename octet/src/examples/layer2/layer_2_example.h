////////////////////////////////////////////////////////////////////////////////
//
// (C) Andy Thomason 2012, 2013
//
// Modular Framework for OpenGLES2 rendering on multiple platforms.
//
// animation example: Drawing an jointed figure with animation
//
// Level: 2
//
// Demonstrates:
//   Collada meshes
//   Collada nodes
//   Collada animation
//
// note this app is not in the octet namespace as it is not part of octet
// and so we need to use  on several classes.



//#include "building.h" // Why was this here? We don't have this file (ask Tolga?)

namespace octet {
  class layer_2_example_app : public app {

    // named resources loaded from collada file
    resources dict;

    // shaders to draw triangles
    bump_shader object_shader;
    bump_shader skin_shader;

    // helper to rotate camera about scene
    mouse_ball ball;

    // helper for drawing text
    text_overlay overlay;

    // helper for picking objects on the screen
    object_picker picker;

    ref<scene> app_scene;
    ref<scene_node> node;
    ref<mesh_instance> mi;
   
  public:
    // this is called when we construct the class
    layer_2_example_app(int argc, char **argv) : app(argc, argv), ball() {
      
      // test the c++ parser
      //parser.parse("int x = 1;");
    }

    // this is called once OpenGL is initialized
    void app_init() {
      object_shader.init(false);
      skin_shader.init(true);

      app_scene =  new scene();
      app_scene->create_default_camera_and_lights();

      node = app_scene->add_scene_node();
      material *mat = new material(vec4(1,0,0,1));
      //mat->make_color(vec4(1,0,0,1), true, false);
      
      //mat = new material("assets/duckCM.gif");
      srand(time(NULL));

      mesh_builder mb;
      
      vec4 leftfront = vec4(-5, 0, 5, 1);
      vec4 rightfront = vec4(5, 0, 5, 1);
      vec4 rightback = vec4(5, 0, -5, 1);
      vec4 leftback = vec4(-5, 0, -5, 1);
      vec4 top = vec4(0, 10, 0, 1);

      mb.add_vertex(leftfront, leftfront.normalize(), 1, 1);
      mb.add_vertex(rightfront, rightfront.normalize(), 0, 1);
      mb.add_vertex(rightback, rightback.normalize(), 0, 0);
      mb.add_vertex(leftback, leftback.normalize(), 1, 0);
      mb.add_vertex(top, top.normalize(), 0, 0);
      mb.add_index(0); mb.add_index(1); mb.add_index(3);
      mb.add_index(1); mb.add_index(2); mb.add_index(3);
      mb.add_index(0); mb.add_index(1); mb.add_index(4);
      mb.add_index(0); mb.add_index(4); mb.add_index(3);
      mb.add_index(1); mb.add_index(2); mb.add_index(4);
      mb.add_index(2); mb.add_index(4); mb.add_index(3);

      mesh *mv = new mesh();

      mb.get_mesh(*mv);

      mv->set_mode(GL_TRIANGLES);
      mi = app_scene->add_mesh_instance(new mesh_instance(node, mv, mat));

      camera_instance *cam = app_scene->get_camera_instance(0);
      cam->set_perspective(0, 45, 1, 10.0f, 1000.0f);
      scene_node *cnode = cam->get_node();
      cnode->access_nodeToParent().translate(0,0,-30);
      mat4t cameraToWorld = cnode->get_nodeToParent();
      //ball.init(this, cameraToWorld.w().length(), 360.0f);
    }

    void HandleCameraControls(mat4t &cameraToWorld)
    {
      static float camX = 0, camY = 15, camZ = 20;
      static float camRX = -30.0f, camRY = 0.0f;

      cameraToWorld.loadIdentity();
      cameraToWorld.translate(camX, camY, camZ);
      cameraToWorld.rotateY(camRY);
      cameraToWorld.rotateX(camRX);

      //Rotations
      const float camRotOffset = 5.0f;

      if (is_key_down(key_left)) {
        camRY += camRotOffset;
      }
      if (is_key_down(key_right)) {
        camRY -= camRotOffset;
      }
      if (is_key_down(key_up)) {
        camRX += camRotOffset;
      }
      if (is_key_down(key_down)) {
        camRX -= camRotOffset;
      }

      //Translations
      const float camTraOffset = 0.5f;
      static vec4 rotatedMovement;// = vec4(0, 0, 0, 1);
      static mat4t cameraRotation;//

      cameraRotation.loadIdentity();
      rotatedMovement = vec4(0, 0, 0, 1);
      cameraRotation.rotateY(camRY);
      //cameraRotation.rotateX(camRX);

      if (is_key_down('W')) {
        //camZ -= camTraOffset;
        rotatedMovement -= vec4(0, 0, camTraOffset, 0);
      }
      if (is_key_down('S')) {
        //camZ += camTraOffset;
        rotatedMovement += vec4(0, 0, camTraOffset, 0);
      }
      if (is_key_down('A')) {
        //camX -= camTraOffset;
        rotatedMovement -= vec4(camTraOffset, 0, 0, 0);
      }
      if (is_key_down('D')) {
        //camX += camTraOffset;
        rotatedMovement += vec4(camTraOffset, 0, 0, 0);
      }
      if (is_key_down('Q')) {
        camY -= camTraOffset;
      }
      if (is_key_down('E')) {
        camY += camTraOffset;
      }


      rotatedMovement = rotatedMovement * cameraRotation;
      camX += rotatedMovement.x();
      camZ += rotatedMovement.z();
    }

    // this is called to draw the world
    void draw_world(int x, int y, int w, int h) {
	#pragma region Viewport Setup
      int vx, vy;
      get_viewport_size(vx, vy);
      // set a viewport - includes whole window area
      glViewport(0, 0, vx, vy);

      // clear the background to black
      glClearColor(0.25f, 0.25f, 0.25f, 1);
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

      // allow Z buffer depth testing (closer objects are always drawn in front of far ones)
      glEnable(GL_DEPTH_TEST);

      GLint param;
      glGetIntegerv(GL_SAMPLE_BUFFERS, &param);
      if (param == 0) {
        // if multisampling is disabled, we can't use GL_SAMPLE_COVERAGE (which I think is mean)
        // Instead, allow alpha blend (transparency when alpha channel is 0)
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        // this is a rather brutal alpha test that cuts off anything with a small alpha.
        #ifndef SN_TARGET_PSP2
          //glEnable(GL_ALPHA_TEST);
          //glAlphaFunc(GL_GREATER, 0.9f);
        #endif
      } else {
        // if multisampling is enabled, use GL_SAMPLE_COVERAGE instead
        glEnable(GL_SAMPLE_ALPHA_TO_COVERAGE);
        glEnable(GL_SAMPLE_COVERAGE);
      }

	#pragma endregion
      if (app_scene && app_scene->get_num_camera_instances())
      {
        int vx = 0, vy = 0;
        get_viewport_size(vx, vy);

        {
          camera_instance *cam = app_scene->get_camera_instance(0);
          scene_node *camnode = cam->get_node();

          HandleCameraControls(camnode->access_nodeToParent());

          //if (is_key_down('W'))
          //{
          //  camnode->access_nodeToParent().translate(0,0,-1);
          //}
          //if (is_key_down('S'))
          //{
          //  camnode->access_nodeToParent().translate(0,0,1);
          //}
          //if (is_key_down('A'))
          //{
          //  camnode->access_nodeToParent().translate(-1,0,0);
          //}
          //if (is_key_down('D'))
          //{
          //  camnode->access_nodeToParent().translate(1,0,0);
          //}
          //if (is_key_down('Q'))
          //{
          //  camnode->access_nodeToParent().translate(0,-1,0);
          //}
          //if (is_key_down('E'))
          //{
          //  camnode->access_nodeToParent().translate(0,1,0);
          //}

          ////camnode->access_nodeToParent().

          //if (is_key_down(key_right))
          //{
          //  camnode->access_nodeToParent().rotateY(-1);
          //}
          //if (is_key_down(key_left))
          //{
          //  camnode->access_nodeToParent().rotateY(1);
          //}
          //if (is_key_down(key_up))
          //{
          //  camnode->access_nodeToParent().rotateX(1);
          //}
          //if (is_key_down(key_down))
          //{
          //  camnode->access_nodeToParent().rotateX(-1);
          //}
          /*mat4t &cameraToWorld = camnode->access_nodeToParent();
          if (is_key_down('W') || is_key_down('S'))
          {
            ball.updateDistance(cameraToWorld.w().length());
          }
          ball.update(cameraToWorld);*/

          app_scene->update(1.0f/30);

          app_scene->render(object_shader, skin_shader, *cam, (float)vx/vy);

        }
        node->access_nodeToParent().rotateZ(1);
        node->access_nodeToParent().rotateX(1);
      }
    }
  };
}